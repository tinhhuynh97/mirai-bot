const { RichEmbed } = require("discord.js");

module.exports.run = async (client, msg, args) => {
    let someone = msg.mentions.users.first() || msg.author;
    let embed = new RichEmbed()
        .setColor("#ff708a")
        .addField("Full Name", someone.username)
        .addField("ID", someone.id)
        .addField("Ngày Tạo", someone.createdAt)
        .setThumbnail("https://i.imgur.com/Wvpzfj2.gif")
        .addField("Avatar", "Profile Avatar")
        .setImage(someone.displayAvatarURL);


    msg.channel.send(embed);
    return;
}
module.exports.help = {
    name: "user"
}