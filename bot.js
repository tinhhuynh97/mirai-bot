const Discord = require('discord.js');
const client = new Discord.Client();
const rules = require('./res/rules.json');
const config = require('./config.json');
const fs = require('fs');
const chalk = require('chalk');

const myToken = config.token;
const prefix = config.prefix;
client.commands = new Discord.Collection();

// server info
const dnhServerId = '420246254254030856';
// channel 
const chatRoomId = '420404621916241930';
const ruleChnId = '450583025537777665';

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}`);
  console.log(`Bot name = ` + chalk.bold.magenta(`${client.user.username}`));
  console.log(`prefix = ` + chalk.bold.red(`${config.prefix}`));
  console.log(`${client.user.username} is online on ${client.guilds.size} servers!`);
  client.user.setGame(`I Love ${config.waifu} <3`);
});


client.on('message', msg => {
  const {
    content,
    channel
  } = msg;
  const {
    member
  } = msg;
  if (msg.channel.type === 'dm') return;
  let args = msg.content.split(" ");
  let command = args[0];
  if (!command.startsWith(prefix)) return;
  let cmd = client.commands.get(command.slice(prefix.length));
  if (cmd) cmd.run(client, msg, args);

});

function setMemberRole(member) {
  let role = member.guild.roles.find("name", "Member");
  member.addRole(role);
}


function isDnhServer(id) {
  return id === dnhServerId;
};

/**  Load Commands */
fs.readdir('./cmds/' + '', (err, files) => {
  if (err) console.error(err);
  let jsfiles = files.filter(f => f.split(".").pop() === "js");
  jsfiles.forEach((f, i) => {
    let props = require(`./cmds/${f}`);
    client.commands.set(props.help.name, props);
  });
});
/** */

client.login(myToken);