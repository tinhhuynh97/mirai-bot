const Discord = require('discord.js');
const client = new Discord.Client();
const rules = require('./res/rules.json');

const myToken = 'Mzk5NzM5NDkyMjkxODM3OTUy.DdX52A.eeSo5C9Y2VSwUV-rucspQWPoB4k'
const myId = '399739492291837952';

// server info
const dnhServerId = '420246254254030856';
// channel 
const chatRoomId = '420404621916241930';
const newMemId = '420495921768431618';

// test server info
const testSv = '431802209567637509';
// test channel
const testCn = '431802209567637511';

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}`);
});

client.on('message', msg => {

});

client.on('guildMemberAdd', member => {
    if (isDnhServer(member.guild.id)) {
        inviteToChatroom(member);
        showRule(member);
    }
});

function showRule(member) {

    sendMsgInChannel(testCn, `Hello <@${member.user.id}>, cảm ơn đã tham gia DNH.\nNếu có thời gian, hãy đọc nội quy của server để trở thành member gương mẫu nhé :v :v`)

    const embed = new Discord.RichEmbed()
        .setTitle('Day Nhau Hoc\'s rules')
        .setAuthor('Bot Siêu Cấp',
            'https://previews.123rf.com/images/popaukropa/popaukropa1604/popaukropa160400044/54870346-cartoon-cute-face-an-yellow-background-gaiety-emotion-sleeping-with-broad-smile-cheerful-festive-cha.jpg')
        .setColor(0xFFE921)
        .setDescription(`Hiện tại nội quy phiên bản tiếng việt đang được cập nhật`);

    for (const rule in rules) {
        if (rule && rules) {
            embed.addField(`${rules[rule].index}`, `${rules[rule].rule}`);
        }
    }

    sendMsgInChannel(testCn, {
        embed
    })

    sendMsgInChannel(newMemId, `Cảm ơn đã dành thời gian đọc nội quy của server.\nĐừng quên sang <#${chatRoomId}> để trò chuyện với mọi người nhé !!!`)

};

function inviteToChatroom(member) {
    sendMsgInChannel(testCn, `Hello <@${member.user.id}>, qua đây để trò chuyện với mọi người nào :3!!`)
};

function sendMsgInChannel(channelId, msg) {
    client.channels.get(channelId).send(msg);
}

function isDnhServer(id) {
    return id === testSv;
};

client.login(myToken);